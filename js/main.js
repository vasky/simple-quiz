import quizData from "./questions.js";

let question = document.getElementsByClassName("questionText");
let ulList = document.getElementsByClassName("ulListQuestions")
let numberQuestion = 0;
let currentQuestions = 0;

let a_question = document.querySelector("#a_question")
let b_question = document.querySelector("#b_question")
let c_question = document.querySelector("#c_question")
let d_question = document.querySelector("#d_question")

const btnSumbmit = document.querySelector(".btnSub")
let canLoadQuestion = false;

function loadQuiz(){
    question[0].innerHTML = quizData[numberQuestion].question;
    const inputsRadio = document.querySelectorAll(".inputQuestion")

    a_question.innerHTML = quizData[numberQuestion].a;
    b_question.innerHTML = quizData[numberQuestion].b;
    c_question.innerHTML = quizData[numberQuestion].c;
    d_question.innerHTML = quizData[numberQuestion].d;

    inputsRadio.forEach(inputRadio  => {
        inputRadio.checked = false;
        canLoadQuestion = false
    })

}

loadQuiz()

btnSumbmit.addEventListener("click", () => {
    const inputsRadio = document.querySelectorAll(".inputQuestion")
    inputsRadio.forEach(inputRadio  => {

        if(inputRadio.checked == true){

            if (quizData[numberQuestion]){
                if(inputRadio.id == quizData[numberQuestion].currentAnswer){
                    currentQuestions++;
                }
            }

            canLoadQuestion = true
            numberQuestion ++;
        }
    })
    if(quizData[numberQuestion] && canLoadQuestion){
        loadQuiz()
    }
    if(quizData.length <= numberQuestion){
        question[0].innerHTML = `Твои результаты: <br>${currentQuestions}/${quizData.length}`;
        ulList[0].style.display = "none"
        btnSumbmit.style.display = "none"
    }
})