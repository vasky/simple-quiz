const quizData = [
    {
        question: "Сколько лет Путину?",
        a: 71,
        b: 67,
        c: 18,
        d: 65,
        currentAnswer: "b"
    },
    {
        question: "Как зовут сына Юрия Дудя?",
        a: "Олег",
        b: "Даниил",
        c: "Вадим",
        d: "Виталий",
        currentAnswer: "b"
    },
    {
        question: "В чем сила?",
        a: "В правде",
        b: "В силе",
        c: "Во власти",
        d: "В доброте",
        currentAnswer: "a"
    },
    {
        question: "Как зовут моего кота?",
        a: "У меня кошка",
        b: "Мурзик",
        c: "Харитон",
        d: "Олег",
        currentAnswer: "c"
    },
    {
        question: "Сколько мне лет?",
        a: 20,
        b: 19,
        c: 14,
        d: 25,
        currentAnswer: "a"
    },
    {
        question: "Сколько у тебя правильных ответов?",
        a: 1,
        b: 5,
        c: 4,
        d: 3,
        currentAnswer: "b"
    }
]

export default quizData;